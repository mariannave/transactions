export default {
  methods: {
    calc() {
      if (this.valor) {
        const valorSaldo = Number(this.saldo);
        let total = 0;

        if (this.action === 'adicionar') {
          total = valorSaldo + Number(this.valor);
        } else {
          total = valorSaldo - Number(this.valor);
        }

        localStorage.setItem('saldo', total);
        this.saldo = total;

        this.setTransactions();
      }
    },
    setTransactions() {
      let today = new Date();
      today = `${today.getDate()}/${today.getMonth() + 1}/${today.getFullYear()}`;

      if (!localStorage.movimentacoes) {
        const dado = {
          date: today,
          movimentacaoDia: [
            {
              valor: this.valor,
              operacao: this.action
            }
          ]
        };

        this.movimentacoes = [dado];
        localStorage.setItem('movimentacoes', JSON.stringify([dado]));
      } else {
        const movimentacoesLista = JSON.parse(localStorage.getItem('movimentacoes'));

        movimentacoesLista.forEach((item) => {
          if (item.date === today) {
            item.movimentacaoDia.push({
              valor: this.valor,
              operacao: this.action
            });
          }
        });

        localStorage.setItem('movimentacoes', JSON.stringify(movimentacoesLista));
        this.movimentacoes = movimentacoesLista;
      }
      // Necessário para limpar os dados do modal.
      this.valor = '';
      this.hide();
    }
  }
};
