import Vue from 'vue';
import Cards from '@/components/Cards';

describe('Cards.vue', () => {
  it('should test add value to account', () => {
    const Constructor = Vue.extend(Cards);
    const vm = new Constructor().$mount();
    // Expect start with 0
    expect(vm.saldo).toBe(0);

    vm.valor = 5.0;
    vm.action = 'adicionar';
    vm.calc();
    const total = vm.valor + vm.saldo;
    expect(String(vm.saldo)).toEqual(total);
  });

  it('should test debit value to account', () => {
    const Constructor = Vue.extend(Cards);
    const vm = new Constructor().$mount();

    vm.valor = 1.0;
    vm.action = 'debitar';
    const total = vm.saldo - vm.valor;
    vm.calc();
    expect(vm.saldo).toEqual(total);
  });

  it('should test negative value to account', () => {
    const Constructor = Vue.extend(Cards);
    const vm = new Constructor().$mount();
    vm.valor = 5.0;
    vm.action = 'debitar';

    vm.calc();
    expect(vm.saldo).toEqual(-1);
  });
});
