# Transações

> Projeto feito em Vue.js. 

## Build Setup

``` bash
# Instalando dependências
npm install

# Comando para rodar a aplicação
npm run dev

# Build para produção com Minificação
npm run build

# Comando para rodar todos os testes
npm test
```